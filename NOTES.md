# Notes for this talk (follow at your peril)

1. Welcome
    - Thanks for coming to this talk, it's all about colours.
    - Code is released under the Apache 2.0 license: re-use and improve at will.
    - Talk is released under the CC-BY-SA license: re-use and improve at will.
2. How do we See Colour?
    - Light is an electromagnetic radiation. Our eyes perceive part of it,
      depending on its frequency or wavelength (the inverse of the frequency).
    - The range between 400 to 700nm is what our eyes are receptive to, this is
      the visible spectrum.
    - Inside our eyes are specialised receptors called cones that are receptive
      to particular wavelengths: ~445, 535 and 575nm.
    - The colour we see depends on what combination of cones are excited and in
      what proportion.
    - Mostly one type of cone: we see blue, green or red.
    - Mostly two types of cone: blue + green => cyan, green + red => yellow.
    - All types of cones excited in the same proportion: we see white or grey.
    - But what happens when blue and red cones are excited but not green ones?
      If our eyes extrapolated to the colour in between we would see green,
      which would be weird as those are the only cones that are not excited.
      Instead, we see magenta, a colour that is not on the spectrum and that
      our eyes make up as a colour between blue and red.
    - Historical aside: magenta was initially called fuchsine and was renamed to
      celebrate the Italian-French victory at the Battle of Magenta fought
      between the French and Austrians on June 4, 1859, near the Italian town
      of Magenta in Lombardy.
3. Additive and Subtractive Colours
    - The way our eyes see colour means that we can trick them into seeing
      a large combination of colours by combining primary colours.
    - Additive colours occur when you create rays of light, in the same way as
      a computer monitor: when several rays of light are close enough together
      and fall on the same cones in the eye, they are combined by our brain in
      rays of a single colour.
    - Red + green => yellow; green + blue => cyan; blue + red => magenta; all
      three together => white.
    - Subtractive colours occur when white light is reflected on a coloured
      surface: some of the light is absorbed, some is reflected.
    - Cyan absorbs red, magenta absorbs green, yellow absorbs blue.
    - Cyan + magenta => blue; magenta + yellow => red; yellow + cyan => green;
      all three together => black.
    - CMYK colours are the same as CMY with added black because combination of
      coloured inks don't always produce pure black.
4. CSS RGB Notation
    - When specifying a colour in CSS, you can use a name such as `pink`.
      However, the number of named colours is limited.
    - The `rgb` function makes it possible to specify a colour by providing
      values for the red, green and blue channels. This is exactly how a
      computer monitor works.
    - Each of the values is an integer between 0 and 255. This maximum value
      that seems a bit random was chosen as it maps directly to the values
      used by computer graphics cards: this is the maximum value that can be
      coded on 8 bits (one byte).
    - Sidetrack into computer bits: each bit is a flag that can be set to 0 or 1,
      in effect a binary digit. When 8 of them are combined, the combined
      variations can take 256 values from 0 to 255. In hexadecimal (base 16),
      this is written FF.
    - Total combinations: more than 16 million colours, which is more than
      enough considering that our perception of colours will vary
      with quality of monitor, light environment, etc.
    - CSS has an hexadecimal notation. In base 16, numbers from 0 to 255 can be
      coded with exactly 2 hexadecimal digits. So the CSS hexadecimal notation
      is a hash followed by 6 hexadecimal digits, one pair of digits for each
      colour channel.
    - CSS has a shortened hexadecimal notation with 3 digits when the digit for
      each channel is repeated.
5. CSS HSL Notation
    - RGB is good for the computer but not very human friendly. This is why CSS
      also offers the HSL notation for Hue, Saturation, Lightness.
    - Hue is a value between 0 and 360 that represents an angle on a colour
      wheel with red at 0 degrees, yellow at 60, green at 120, cyan at 180,
      blue at 240 and magenta at 300.
    - Saturation is a value between 0% and 100% with 0% being grey, 100% being
      the fully saturated colour.
    - Lightness is a value between 0% and 100% with 0% being black, 50% being
      the fully saturated colour and 100% being white.
    - Way to use: choose the hue, set saturation to 100%, lightness to 50% to
      get a fully saturated colour.
    - Adjust saturation and lightness to create tints and shades of the base
      colour.
6. Opacity (aka Alpha Channel)
    - Opacity is used to mimic partially transparent materials, that is a
      material that reflects some of the light and lets some light through
      from elements that are placed behind.
    - Opacity is specified using variations of the `rgb` and `hsl` functions:
      the `rgba` and `hsla` functions have a fourth parameter which is a percentage
      from 0%: totally transparent to 100%: totally opaque. You can also use
      a decimal value between 0 and 1.
7. Accessibility
    - Some accessibility concepts to bear in mind with colours.
8. Contrast
    - Contrast is key to make your content legible.
    - Higher contrast is better but a bigger font can also do the trick.
    - See WCAG 1.4.
9. Colour Blindness
    - Affects primarily men, which is probably why it's well understood.
    - Most people see distorted colours, a small number just see black and white.
    - Use different shapes or lightness, add words, etc.
    - Rule of thumb: if it works in black and white, it will probably work for
      all colour blind people.
    - Rule of thumb: yellows and blues are usually well distinguished.
10. Tools
    - A few tools (with demos!) that can be useful to work with colour.
11. ColorZilla
    - Pick a colour from anywhere in a page, very useful to get colours from
      photographs.
12. Paletton
    - Once you have a colour, create a whole palette.
    - Shades and tints.
    - Colour scheme: monochromatic, adjacent, triad, tetrad, free style.
13. WebAIM Contrast Checker
    - Check colour contrast ratio against WCAG.
    - Note the normal text and large text variants.
14. Colourblind Web Page Filter
    - Load a page, check for a particular type of colour blindness.
15. References
    - Bits and bobs used to write this talk.
16. Questions
