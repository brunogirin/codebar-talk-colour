# codebar-talk-colour

Talk for codebar monthly June 2019 on how colours work on the web. The code is
released under the [Apache 2.0 license](./LICENSE) while the content is
released under the
[Creative Commons Attribution-ShareAlike license](https://creativecommons.org/licenses/by-sa/4.0/).
In other words, feel free to use any of the code in your own projects and feel
free to give this talk in any setting. If you modify the content, please give
appropriate credit and share the modified talk under the same license.

The online version of this talk is available as a
[GitLab page](https://brunogirin.gitlab.io/codebar-talk-colour/). To transition
between slides, click on the arrows on the sides, use the `left` and `right`
keys on the keyboard or swipe on a mobile device. The `home` and `end` keys
move to the first and last slide respectively.

## Project setup

The slide deck is built as a [Vue.js app](https://vuejs.org/). To run it locally,
you will need [node.js](https://nodejs.org/en/) installed. You can use either
`npm` or `yarn` to build and run it. The instructions below are for `yarn`.

The project is setup to deploy automatically using GitLab CI. The CI script
can be found in the `.gitlab-cy.yml` file.

### Install dependencies
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```
