module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? '/codebar-talk-colour/'
    : '/'
}
